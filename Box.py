class Box:
	def __init__(self, capacity=None, isopen=None):
		self.capacity = capacity
		self.isopen = isopen
	
	@staticmethod
	def construction(d):
		c = d.get("capacity", None)
		i = d.get("isopen", None)
		return Box(capacity=c, isopen=i)
